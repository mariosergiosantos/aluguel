@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Oops!</strong><br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

@if (Session::has('success'))
<div class="alert alert-dismissable alert-success">
    {{Session::get('success')}}
</div>
@endif

@if (Session::has('info'))
<div class="alert alert-dismissable alert-info">
    {{Session::get('info')}}
</div>
@endif

@if (Session::has('warning'))
<div class="alert alert-dismissable alert-warning">
    {{Session::get('warning')}}
</div>
@endif

@if (Session::has('danger'))
<div class="alert alert-dismissable alert-danger">
    {{Session::get('danger')}}
</div>
@endif
