@extends('auth.layouts.main')
@section('conteudo')
<body class="login-page">
    <div class="login-box">
        <div class="login-logo">
            <a href="{{url('#')}}"><b>Alugue</b>JA</a>
        </div><!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg">Entre para iniciar sua sessão
            <form method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group has-feedback">
                    <input type="text" class="form-control" name="email" placeholder="Email"/>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" class="form-control" name="password" placeholder="Password"/>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="row">
                    <div class="col-xs-8">    
                        <div class="checkbox icheck">
                            <label>
                                <input type="checkbox" name="remember"> Lembre-me
                            </label>
                        </div>                        
                    </div><!-- /.col -->
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Entrar</button>
                    </div><!-- /.col -->
                </div>
            </form>

            <div class="social-auth-links text-center">
              <!--<p>- OR -</p>-->
                <a href="{{url('auth/facebook')}}" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Entrar usando Facebook</a>
                <a href="{{url('auth/google')}}" class="btn btn-block btn-social btn-google-plus btn-flat"><i class="fa fa-google-plus"></i> Entrar usando Google+</a>
            </div><!-- /.social-auth-links -->

            <a href="{{url('password/email')}}">Esqueci minha senha</a><br>
            <!--<a href="register.html" class="text-center">Register a new membership</a>-->

        </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <script src="{{url('plugins/jQuery/jQuery-2.1.3.min.js')}}"></script>    
    <script src="{{url('bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
</body>
@endsection
