<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ url(Auth::user()->url_image) }}" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>{{Auth::user()->name}}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Pesquisar..."/>
                <span class="input-group-btn">
                    <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">NAVEGAÇÃO PRINCIPAL</li>
            <li>
                <a href="{{url('painel/dashboard')}}">
                    <i class="fa fa-dashboard"></i> <span>{{trans('messages.page.dashboard')}}</span> <i class="fa pull-right"></i>
                </a>
            </li>

            <li>
                <a href="{{url('painel/fornecedor')}}">
                    <i class="fa fa-dashboard"></i> <span> {{trans('messages.page.provider')}}</span> <i class="fa pull-right"></i>
                </a>
            </li>

            <li>
                <a href="{{url('painel/cliente')}}">
                    <i class="fa fa-dashboard"></i> <span>{{trans('messages.page.client')}}</span> <i class="fa pull-right"></i>
                </a>
            </li>

            <li>
                <a href="{{url('painel/produto')}}">
                    <i class="fa fa-dashboard"></i> <span>{{trans('messages.page.product')}}</span> <i class="fa pull-right"></i>
                </a>
            </li>

            <li>
                <a href="{{url('painel/aluguel')}}">
                    <i class="fa fa-dashboard"></i> <span>{{trans('messages.page.rent')}}</span> <i class="fa pull-right"></i>
                </a>
            </li>

            <!--<li>
                <a href="{{url('painel/faturamento')}}">
                    <i class="fa fa-dashboard"></i> <span>Faturamento</span> <i class="fa pull-right"></i>
                </a>
            </li>-->

            <li>
                <a href="{{url('painel/membros')}}">
                    <i class="fa fa-dashboard"></i> <span>{{trans('messages.page.member')}}</span> <i class="fa pull-right"></i>
                </a>
            </li>

            <li>
                <a href="{{url('painel/configuracao')}}">
                    <i class="fa fa-dashboard"></i> <span>{{trans('messages.page.configuration')}}</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>