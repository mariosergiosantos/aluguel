<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>@yield('titulo')</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="{{url('bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <link href="{{url('plugins/daterangepicker/daterangepicker-bs3.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('dist/css/AdminLTE.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{url('dist/css/skins/_all-skins.min.css')}}" rel="stylesheet" type="text/css" />
        <script src="{{url('plugins/jQuery/jQuery-2.1.3.min.js')}}"></script>
        @yield('script')
    </head>
    <body class="skin-blue">
        <div class="wrapper">

            @include('layouts::topo')
            @include('layouts::sidebar')
            @yield('conteudo')

            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 1.0
                </div>
                <strong>Copyright &copy; 2014-2016 <a target="_blank" href="http://www.plusvision.com.br">Plusvision | Consultoria e desenvolvimento de sistemas</a>.</strong> All rights reserved.
            </footer>
        </div>
        <script src="{{url('bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>

        @yield('script_final')
        <script src="{{url('plugins/fastclick/fastclick.min.js')}}"></script>
        <script src="{{url('dist/js/app.min.js')}}" type="text/javascript"></script>
        <script src="{{url('plugins/daterangepicker/daterangepicker.js')}}" type="text/javascript"></script>
        <script src="{{url('plugins/datepicker/bootstrap-datepicker.js')}}" type="text/javascript"></script>
        <script src="{{url('plugins/slimScroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
        <script src="{{url('dist/js/pages/dashboard2.js')}}" type="text/javascript"></script>
        <script src="{{url('dist/js/demo.js')}}" type="text/javascript"></script>

        <script type="text/javascript">
        //Money Euro
        $("[data-mask]").inputmask();
        
        $('#test').inputmask('[9][9][9][9]');
      
    </script>

    </body>
</html>