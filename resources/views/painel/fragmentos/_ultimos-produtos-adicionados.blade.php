<div class="col-md-4">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">
                Últimos Produtos Adicionado</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                <ul class="products-list product-list-in-box">

                    @foreach($produtos as $produto)
                    <li class="item">
                        <div class="product-img">
                            <img src="{{$produto->url_img}}" alt="Product Image"/>
                        </div>
                        <div class="product-info">
                            <a href="{{url('painel/produto')}}/{{$produto->id}}" class="product-title">{{$produto->nome}} <span class="label label-warning pull-right">R$ {{$produto->preco_unitario}}</span></a>
                            <span class="product-description">
                                {{$produto->descricao}}
                            </span>
                        </div>
                    </li>
                    @endforeach
                </ul>
            </div>
            <div class="box-footer text-center">
                <a href="javascript::;" class="uppercase">Visualizar Todos Produtos</a>
            </div>
        </div>
    </div>
</div>