<div class='col-md-4'>
    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title">Fornecedores</h3>
            <div class="box-tools pull-right">
                <span class="label label-danger">{{count($fornecedores)}} Fornecedores</span>
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
        </div>
        <div class="box-body no-padding">
            <ul class="users-list clearfix">
                @foreach($fornecedores as $fornecedor)
                <li>
                    <img src="{{$fornecedor->url_img}}" alt="User Image"/>
                    <a class="users-list-name" href="#" title="{{$fornecedor->nome}}">{{$fornecedor->nome}}</a>
                    <span class="users-list-date">Today</span>
                </li>
                @endforeach
            </ul>
        </div>
        <div class="box-footer text-center">
            <a href="{{url('painel/fornecedores')}}" class="uppercase">Visualizar Todos Fornecedores</a>
        </div>
    </div>
</div>