<div class="row">
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-aqua"><i class="ion-person-stalker"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">Fornecedores</span>
				<span class="info-box-number">{{count($fornecedores)}}</span>
			</div>
		</div>
	</div>
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-red"><i class="ion ion-person-add"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">Clientes</span>
				<span class="info-box-number">{{count($clientes)}}</span>
			</div>
		</div>
	</div>

	<div class="clearfix visible-sm-block"></div>

	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">Vendas</span>
				<span class="info-box-number">{{count($vendas)}}</span>
			</div>
		</div>
	</div>
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-yellow"><i class="ion ion-ios-pricetag-outline"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">Produtos</span>
				<span class="info-box-number">{{count($produtos)}}</span>
			</div>
		</div>
	</div>
</div>