<div class="box box-info">
	<div class="box-header with-border">
		<h3 class="box-title">Últimas Produtos Vendidos</h3>
		<div class="box-tools pull-right">
			<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
			<button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
		</div>
	</div>
	<div class="box-body">
		<div class="table-responsive">
			<table class="table no-margin">
				<thead>
					<tr>
						<th>Order ID</th>
						<th>Item</th>
						<th>Horário</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>
					@foreach($ultimosProdutosVendidos as $produto)
					<tr>
						<td><a href="pages/examples/invoice.html">{{$produto->id}}</a></td>
						<td>{{$produto->nome}}</td>
						<td>{{$produto->horario}}</td>
						<td><span class="label label-success">Shipped</span></td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
	<div class="box-footer clearfix">
		<a href="javascript::;" class="btn btn-sm btn-info btn-flat pull-left">Cadastrar Venda</a>
		<a href="javascript::;" class="btn btn-sm btn-default btn-flat pull-right">Visualizar Todas as Vendas</a>
	</div>
</div>
</div>