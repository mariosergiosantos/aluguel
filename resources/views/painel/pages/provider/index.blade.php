@extends('layouts::main')

@section('conteudo')
<div class="content-wrapper">

    <section class="content-header">
        <h1>
            Fornecedores
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('painel/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Fornecedores</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header">
                <a class="btn btn-sm btn-info btn-flat col-lg-2" href="fornecedor/create">Novo</a>
                <div class="box-tools">
                    <form class="form-horizontal col-lg-offset-3" role="form" 
                          method="get" action="{{url('painel/fornecedor')}}">

                        <div class="input-group">
                            <input type="text" name="search" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Buscar"/>
                            <div class="input-group-btn">
                                <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <div class="col-lg-12">
                    @include('errors.mensagem')
                </div>
                <table class="table table-hover" id="table_fornecedores">
                    <tr>
                        <th>ID</th>
                        <th>Nome</th>
                        <th>E-mail</th>
                        <th>Telefone</th>
                        <th>Estado</th>
                    </tr>

                    @foreach($fornecedores as $fornecedor)
                    <tr>
                        <td><a href="{{url('painel/fornecedor')}}/{{$fornecedor->id}}">{{$fornecedor->id}}</a></td>
                        <td><a href="{{url('painel/fornecedor')}}/{{$fornecedor->id}}">{{$fornecedor->nome}}
                            </a></td>
                        <td>{{$fornecedor->email}}</td>
                        <!--<td><span class="label label-success">Approved</span></td>-->
                        <td>{{$fornecedor->telefone}}</td>
                        <td>{{$fornecedor->estado}}</td>
                    </tr>
                    @endforeach

                    <!--fornecedores->render()-->
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </section>
</div>

@endsection