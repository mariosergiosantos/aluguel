@extends('layouts::main')

@section('conteudo')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Detalhes fornecedor
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('painel/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{url('painel/fornecedor')}}">Fornecedores</a></li>
            <li class="active">Detalhar</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-body table-responsive no-padding">
                <div class="col-lg-12 main-chart">
                    <div class="col-lg-10 col-lg-offset-1">
                        <div class="thumbnail">
                            <img class="img-responsive" src="{{$fornecedor->url_img}}" height="300px" width="200px" alt="">
                            <div class="caption-full">
                                <h4 class="pull-right">Nome</h4>
                                <h4><a href="#"> asa</a>
                                </h4>
                                <p>dsd</p>
                            </div>
                        </div>
                        <div class="col-lg-offset-5">
                            <form method="POST" action="{{url('painel/fornecedor')}}/{{$fornecedor->id}}" accept-charset="UTF-8">
                                <a class="btn btn-primary btn-mini" href="{{url('painel/fornecedor')}}/{{$fornecedor->id}}/edit">Editar</a> 
                                <input name="_method" type="hidden" value="DELETE">
                                {!! csrf_field() !!}
                                <input class="btn btn-danger btn-mini" type="submit" value="Excluir">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
</div>

<style>
    form{
        padding-top: 20px;
    }
</style>
@endsection