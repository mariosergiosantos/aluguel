@extends('layouts::main')

@section('script')
<script src="{{url('js/jquery/jquery.maskedinput.js')}}"></script>
@endsection

@section('titulo')
Configurações
@endsection

@section('conteudo')

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Configurações
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Configurações</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-body table-responsive no-padding">
                <div class="col-lg-12 main-chart">
                    @include('errors.mensagem')
                    <div class="row">
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h3>
                                        {{$configuracao->idioma['nome']}}
                                    </h3>
                                    <p>
                                        Idioma
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-bag"></i>
                                </div>
                                <a href="#" class="small-box-footer">

                                </a>
                            </div>
                        </div><!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-green">
                                <div class="inner">
                                    <h3>
                                        {{$configuracao->tema['nome']}}
                                    </h3>
                                    <p>
                                        Tema
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-stats-bars"></i>
                                </div>
                                <a href="#" class="small-box-footer">

                                </a>
                            </div>
                        </div><!-- ./col -->
                    </div>
                    <a href="{{url('painel/configuracao/edit')}}" class="btn btn-primary col-lg-12">Editar</a>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
</div>

<style>
    form{
        padding-top: 20px;
    }
</style>
@endsection