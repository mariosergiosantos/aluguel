@extends('layouts::main')

@section('conteudo')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Configuraçao
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Configurações</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-body table-responsive no-padding">
                <div class="col-lg-12 main-chart">
                    <form class="form-horizontal col-lg-offset-3" role="form"
                          method="post" action="{{url('painel/configuracao/edit')}}" accept-charset="UTF-8">
                        <input name="_method" type="hidden" value="PUT">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="form-group">
                            <div class="col-md-8">
                                <select name="idioma_id" class="form-control">
                                    @foreach($idiomas as $idioma)
                                    <option value="{{$idioma->id}}">{{$idioma->nome}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8">
                                <select name="tema_id" class="form-control">
                                    @foreach($temas as $tema)
                                    <option value="{{$tema->id}}" alt="{{$tema->nome}}">{{$tema->nome}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2">
                                <button type="submit" class="btn btn-primary col-md-4">
                                    Salvar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
</div>

<style>
    form{
        padding-top: 20px;
    }
</style>
@endsection