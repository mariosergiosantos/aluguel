@extends('layouts::main')

@section('conteudo')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Aluguel
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('painel/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Aluguel</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header">
                <a class="btn btn-sm btn-info btn-flat col-lg-2" href="aluguel/create">Novo</a>
                <div class="box-tools">
                    <form class="form-horizontal col-lg-offset-3" role="form" 
                          method="get" action="{{url('painel/aluguel')}}">

                        <div class="input-group">
                            <input type="text" name="search" 
                                   class="form-control input-sm pull-right" style="width: 150px;" value="{{ \Request::get('search') }}" placeholder="Buscar"/>
                            <div class="input-group-btn">
                                <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <div class="col-lg-12">
                    @include('errors.mensagem')
                </div>
                <table class="table table-hover" id="table_fornecedores">
                    <tr>
                        <th>ID</th>
                        <th>Data do pedido</th>
                        <th>Data de entrega</th>
                        <th>Produto</th>
                        <th>Cliente</th>
                        <th>Quantidade</th>
                        <th>Preço</th>
                    </tr>

                    @foreach($vendas as $venda)
                    <tr>
                        <th>#{{$venda->id}}</th>
                        <th>{{$venda->data_pedido}}</th>
                        <th>{{$venda->data_entrega}}</th>
                        <th>{{$venda->produto['nome']}}</th>
                        <th>{{$venda->cliente['nome']}}</th>
                        <th>{{$venda->id}}</th>
                        <th>R$ 200,00</th>
                    </tr>
                    @endforeach
                </table>
                <div class="col-lg-offset-5">

                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </section>

    <style type="text/css">
        .red{
            color: red;
        }
    </style>
</div>

@endsection