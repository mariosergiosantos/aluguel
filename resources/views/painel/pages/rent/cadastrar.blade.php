@extends('layouts::main')

@section('conteudo')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Aluguel
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('painel/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Aluguel</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-body table-responsive no-padding">
                <div class="col-lg-12 main-chart">

                    <form class="form-horizontal col-lg-offset-3" role="form"
                          method="post" action="{{url('painel/aluguel')}}">
                        {!! csrf_field() !!}

                        <div class="form-group">
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="data_pedido"  id="dataPedido"
                                       value="{{ old('dataPedido') }}" placeholder="Data do pedido" maxlength="10" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="data_entrega"  id="dataEntrega"
                                       value="{{ old('dataEntrega') }}" placeholder="Data de entrega" maxlength="10" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8">
                                <select name="produto_id" class="form-control">
                                    @foreach($produtos as $produto)
                                    <option value="{{$produto->id}}">{{$produto->nome}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8">
                                <select name="cliente_id" class="form-control">
                                    @foreach($clientes as $cliente)
                                    <option value="{{$cliente->id}}">{{$cliente->nome}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="quantidade" id="test"
                                       value="{{ old('quantidade') }}" placeholder="Quantidade">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2">
                                <button type="submit" class="btn btn-primary col-md-4">
                                    Salvar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
</div>

<style>
    form{
        padding-top: 20px;
    }
</style>

@section('script_final')
<!-- InputMask -->
<script src="{{url('plugins/input-mask/jquery.inputmask.js')}}" type="text/javascript"></script>
<script src="{{url('plugins/input-mask/jquery.inputmask.date.extensions.js')}}" type="text/javascript"></script>
<script src="{{url('plugins/input-mask/jquery.inputmask.extensions.js')}}" type="text/javascript"></script>
@endsection

@endsection