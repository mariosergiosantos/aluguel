@extends('layouts::main')

@section('conteudo')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Dashboard
            <small>Version 1.0</small>
        </h1>
    </section>

    <section class="content">
        @include('fragmentos::_information')
        {{--@include('painel::fragmentos._relatorio-mensal')--}}

        <div class="row">
            {{--@include('painel::fragmentos._visitas')--}}
            {{--@include('painel::fragmentos._blocos')--}}
        </div>

        <div class='row'>

            <div class="col-md-7 pull-left">
                @include('fragmentos::_to-do-list')
            </div>

            <div class="col-md-5 pull-right">
                @include('fragmentos::_calendario')
            </div>
            {{--@include('painel::fragmentos._chat')--}}
            {{--@include('painel::fragmentos._membros')
            @include('painel::fragmentos._fornecedores')--}}
        </div>

        <div class="row">
            <div class="">
                {{--@include('painel::fragmentos._ultimos-produtos-vendidos')--}}
                {{--@include('painel::fragmentos._ultimos-produtos-adicionados')--}}
                </section>
            </div>

            @endsection