@extends('layouts::main')

@section('script')
<script src="{{url('js/jquery/jquery.maskedinput.js')}}"></script>
<script>
$(function () {
    $("#preco").mask("9999,99");
    $("#estoque").mask("9999");
});
</script>
@endsection

@section('conteudo')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Produtos
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{url('painel/produtos')}}">Produtos</a></li>
            <li class="active">Cadastrar</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-body table-responsive no-padding">
                <div class="col-lg-12 main-chart">
                    <form class="form-horizontal col-lg-offset-3" role="form"
                          method="post" action="{{url('painel/produto')}}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="form-group">
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="nome" 
                                       value="{{ old('name') }}" placeholder="Nome" maxlength="40">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8">
                                <select name="fk_fornecedor" class="form-control">
                                    @foreach($fornecedores as $fornecedor)
                                    <option value="{{$fornecedor->id}}">{{$fornecedor->nome}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8">
                                <select name="fk_categoria" class="form-control">
                                    @foreach($categorias as $categoria)
                                    <option value="{{$categoria->id}}" alt="{{$categoria->descricao}}">{{$categoria->nome}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="preco_unitario" id="preco"
                                       value="{{ old('preco') }}" placeholder="Preço">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8">
                                <input type="number" class="form-control" name="estoque" id="estoque"
                                       value="{{ old('estoque') }}" placeholder="Estoque">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8">
                                <textarea class="form-control" placeholder="Descricao" rows="5" name="descricao"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2">
                                <button type="submit" class="btn btn-primary col-md-4">
                                    Salvar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
</div>

<style>
    form{
        padding-top: 20px;
    }
</style>
@endsection