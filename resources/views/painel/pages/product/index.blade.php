@extends('layouts::main')

@section('conteudo')
<div class="content-wrapper">

    <section class="content-header">
        <h1>
            Produtos
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('painel/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Produtos</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header">
                <a class="btn btn-sm btn-info btn-flat col-lg-2" href="produto/create">Novo</a>
                <div class="box-tools">
                    <form class="form-horizontal col-lg-offset-3" role="form" 
                          method="get" action="{{url('painel/produto')}}">

                        <div class="input-group">
                            <input type="text" name="search" 
                                   class="form-control input-sm pull-right" style="width: 150px;" value="{{ \Request::get('search') }}" placeholder="Buscar"/>
                            <div class="input-group-btn">
                                <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <div class="col-lg-12">
                    @include('errors.mensagem')
                </div>
                <table class="table table-hover" id="table_fornecedores">
                    <tr>
                        <th>ID</th>
                        <th>Nome</th>
                        <th>Descrição</th>
                        <th>Categoria</th>
                        <th>Fornecedor</th>
                        <th>Preço</th>
                        <th>Estoque</th>
                    </tr>

                    @foreach($produtos as $produto)
                    <tr>
                        <td><a href="{{url('painel/produto')}}/{{$produto->id}}"> {{$produto->id}} </a></td>
                        <td><a href="{{url('painel/produto')}}/{{$produto->id}}"> {{$produto->nome_produto}} </a></td>
                        <td>{{$produto->descricao}}</td>
                        <!--<td><span class="label label-success">Approved</span></td>-->
                        <td>{{$produto->nome_categoria}}</td>
                        <td>{{$produto->nome_fornecedor}}</td>
                        <td>R$ {{number_format($produto->preco_unitario, 2, ",",".")}}</td>

                        <td @if($produto->estoque <= 30)  class='red' @endif>{{$produto->estoque}}</td>

                    </tr>
                    @endforeach
                </table>
                <div class="col-lg-offset-5">
                    {!! $produtos->render() !!}	
                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </section>

    <style type="text/css">
        .red{
            color: red;
        }
    </style>
</div>

@endsection