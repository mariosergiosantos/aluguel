@extends('layouts::main')

@section('conteudo')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Clientes
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Clientes</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header">
                <a class="btn btn-sm btn-info btn-flat col-lg-2" href="cliente/create">Novo</a>
                <div class="box-tools">

                    <form class="form-horizontal col-lg-offset-3" role="form" 
                          method="get" action="{{url('painel/cliente')}}">

                        <div class="input-group">
                            <input type="text" name="search" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Buscar"/>
                            <div class="input-group-btn">
                                <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <div class="col-lg-12">
                    @include('errors.mensagem')
                </div>
                <table class="table table-hover" id="table_cliente">
                    <tr>
                        <th>ID</th>
                        <th>Nome</th>
                        <th>E-mail</th>
                        <th>Telefone</th>
                        <th>Estado</th>
                    </tr>

                    @foreach($clientes as $cliente)
                    <tr>
                        <td><a href="{{url('painel/cliente')}}/{{$cliente->id}}"> {{$cliente->id}} </a></td>
                        <td><a href="{{url('painel/cliente')}}/{{$cliente->id}}"> {{$cliente->nome}} </a></td>
                        <td>{{$cliente->email}}</td>
                        <td>{{$cliente->telefone}}</td>
                        <td>{{$cliente->estado}}</td>
                    </tr>
                    @endforeach
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </section>
</div>

@endsection