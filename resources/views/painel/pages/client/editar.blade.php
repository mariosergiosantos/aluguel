@extends('layouts::main')

@section('script')
<script src="{{url('js/jquery/jquery.maskedinput.js')}}"></script>
<script>
$(function () {
    $("#cnpj").mask("99.999.999/9999-99");
    $("#telefone").mask("(99) 9999-9999");
    $("#cep").mask("99999-999");
});
</script>
@endsection

@section('conteudo')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Cliente
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{url('painel/cliente')}}">Clientes</a></li>
            <li class="active">Editar</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-body table-responsive no-padding">
                <div class="col-lg-12 main-chart">
                    <form class="form-horizontal col-lg-offset-3" role="form"
                          method="post" action="{{url('painel/cliente')}}/{{$cliente->id}}" accept-charset="UTF-8">
                        <input name="_method" type="hidden" value="PUT">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="form-group">
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="nome" 
                                       value="{{ $cliente->nome }}" placeholder="Nome" maxlength="40">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="email"  id="email"
                                       value="{{ $cliente->email }}" placeholder="E-mail" maxlength="40">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8">
                                <select name="tipo_cliente" class="form-control">
                                    <option value="1">Fisíca</option>
                                    <option value="2">Jurídica</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="cnpj" id="cnpj"
                                       value="{{ $cliente->documento }}" placeholder="CNPJ">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="telefone" id="telefone"
                                       value="{{ $cliente->telefone }}" placeholder="Telefone">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="estado" id="estado"
                                       value="{{ $cliente->estado }}" placeholder="Estado">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="cidade"
                                       value="{{ $cliente->cidade }}" placeholder="Cidade">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="cep" id="cep"
                                       value="{{ $cliente->cep }}" placeholder="CEP">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="endereco"
                                       value="{{ $cliente->endereco }}" placeholder="Endereço">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2">
                                <button type="submit" class="btn btn-primary col-md-4">
                                    Salvar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
</div>

<style>
    form{
        padding-top: 20px;
    }
</style>
@endsection