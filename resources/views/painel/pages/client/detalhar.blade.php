@extends('layouts::main')

@section('conteudo')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Cliente
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('painel/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{url('painel/cliente')}}">Cliente</a></li>
            <li class="active">Detalhar</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-body table-responsive no-padding">
                <div class="col-lg-12 main-chart">
                    <div class="col-lg-10 col-lg-offset-1">
                        <div class="thumbnail">
                            <img class="img-responsive" src="{{$cliente->url_img}}" height="300px" width="200px" alt="">
                            <div class="caption-full">
                                <h4 class="pull-right">R$ </h4>
                                <h4><a href="#"> {{$cliente->nome}}</a>
                                </h4>
                                <p>{{$cliente->descricao}}</p>
                            </div>

                            <div class="col-lg-offset-5">
                                <form method="POST" action="{{url('painel/cliente')}}/{{$cliente->id}}" accept-charset="UTF-8">
                                    <a class="btn btn-primary btn-mini" href="{{url('painel/cliente')}}/{{$cliente->id}}/edit">Editar</a> 
                                    <input name="_method" type="hidden" value="DELETE">
                                    {!! csrf_field() !!}
                                    <input class="btn btn-danger btn-mini" type="submit" value="Excluir">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
</div>

<style>
    form{
        padding-top: 20px;
    }
</style>
@endsection