<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

return [

    "configuration" => [
        "success" => "Configurações atualizadas com sucesso!",
        "error" => "Erro oa atualizar as configurações!"
    ],
    "nofound" => [
        "product" => "Produto não encontrado!",
        "provider" => "Fornecedor não encontrado!",
        "client" => "Cliente não encontrado!",
        "member" => "Membro não encontrado!",
        "rent" => "Aluguel não encontrado!",
    ],
    "store" => [
        "product" => "Produto cadastrado!",
        "provider" => "Fornecedor cadastrado!",
        "client" => "Cliente cadastrado!",
        "member" => "Membro cadastrado!",
        "rent" => "Aluguel cadastrado!",
    ],
    "update" => [
        "product" => "Produto atualizado!",
        "provider" => "Fornecedor atualizado!",
        "client" => "Cliente atualizado!",
        "member" => "Membro atualizado!",
        "rent" => "Aluguel atualizado!",
    ],
    "destroy" => [
        "product" => "Produto excluído!",
        "provider" => "Fornecedor excluído!",
        "client" => "Cliente excluído!",
        "member" => "Membro excluído!",
        "rent" => "Aluguel excluído!",
    ],
    "page" => [
        "product" => "Produto",
        "provider" => "Fornecedor",
        "client" => "Cliente",
        "member" => "Membro",
        "rent" => "Aluguel",
        "dashboard" => "Painel",
        "member" => "Membro",
        "configuration" => "Configuração",
    ],
];
