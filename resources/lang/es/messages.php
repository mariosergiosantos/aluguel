<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

return [

    "configuration" => [
        "success" => "Ha actualizado correctamente la configuración!",
        "error" => "Error al actualizar la configuración!"
    ],
    "nofound" => [
        "product" => "Producto no encontrado!",
        "provider" => "Proveedor no encontrado!",
        "client" => "El cliente no encontrado!",
        "member" => "Miembro no encontrado!",
        "rent" => "Alquilar No encontrado!",
    ],
    "store" => [
        "product" => "Producto registrado!",
        "provider" => "Proveedor de registro!",
        "client" => "Cliente registrado!",
        "member" => "Usuario Registrado!",
        "rent" => "Renta registrado!",
    ],
    "update" => [
        "product" => "Actualizado producto!",
        "provider" => "Proveedor Actualizado!",
        "client" => "Client actualiza!",
        "member" => "Miembro actualiza!",
        "rent" => "Rent-fecha!",
    ],
    "destroy" => [
        "product" => "Producto eliminados!",
        "provider" => "Proveedor eliminados!",
        "client" => "Cliente borrado!",
        "member" => "Miembro borrado!",
        "rent" => "Alquiler eliminados!",
    ],
    "page" => [
        "product" => "Producto",
        "provider" => "Proveedor",
        "client" => "Cliente",
        "member" => "Miembro",
        "rent" => "Alquiler",
        "dashboard" => "Panel de control",
        "member" => "Miembro",
        "configuration" => "Configuración",
    ],
];
