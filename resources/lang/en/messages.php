<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

return [

    "configuration" => [
        "success" => "Successfully updated settings!",
        "error" => "Error updating settings!"
    ],
    "nofound" => [
        "product" => "Product not found!",
        "provider" => "Provider not found!",
        "client" => "Client not found!",
        "member" => "Member not found!",
        "rent" => "Rent not found!",
    ],
    "store" => [
        "product" => "Product registered!",
        "provider" => "Provider registered!",
        "client" => "Client registered!",
        "member" => "Member registered!",
        "rent" => "Rent registered!",
    ],
    "update" => [
        "product" => "Product updated!",
        "provider" => "Provider updated!",
        "client" => "Client updated!",
        "member" => "Member updated!",
        "rent" => "Rent updated!",
    ],
    "destroy" => [
        "product" => "Product excluded!",
        "provider" => "Provider excluded!",
        "client" => "Client excluded!",
        "member" => "Member excluded!",
        "rent" => "Rent excluded!",
    ],
    "page" => [
        "product" => "Product",
        "provider" => "Provider",
        "client" => "Client",
        "member" => "Member",
        "rent" => "Rent",
        "dashboard" => "Dashboard",
        "member" => "Member",
        "configuration" => "configuration",
    ],
];
