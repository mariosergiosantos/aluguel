<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Status
 *
 * @author Mario
 */
class Fornecedores extends Model {

    protected $table = 'fornecedores';
    
    protected $fillable = 
        [
            'nome',
            'email',
            'telefone',
            'cnpj',
            'estado',
            'cidade',
            'cep',
            'pais',
            'endereco',
            'created_at',
            'updated_at'
        ];

}
