<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of Produtos
 *
 * @author Mário
 */
class Produtos extends Model {

    protected $table = 'produtos';
    
    protected $fillable = 
        [
            'nome',
            'fk_fornecedor',
            'fk_categoria',
            'preco_unitario',
            'estoque',
            'descricao',
            'created_at',
            'updated_at'
        ];
    
    public function categoria(){
        return $this->belongsTo(Categorias::class, 'fk_categoria');
    }
    
   public function fornecedor(){
       return $this->belongsTo(Fornecedores::class, 'fk_fornecedor');
   }

}
