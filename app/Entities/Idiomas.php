<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of Produtos
 *
 * @author Mário
 */
class Idiomas extends Model {

    protected $table = 'idiomas';
    
    protected $timestamp = false;
    
    const INGLES = 1;
    const PORTUGUES = 2;
    const ESPANHOL = 3;


    protected $fillable = 
        [  ];
    
}
