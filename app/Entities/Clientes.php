<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of Produtos
 *
 * @author Mário
 */
class Clientes extends Model {

    protected $table = 'clientes';
    
    protected $fillable = 
        [
            'nome',
            'email',
            'documento',
            'telefone',
            'estado',
            'cidade',
            'cep',
            'pais',
            'endereco',
            'created_at',
            'updated_at'
        ];

}
