<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of Produtos
 *
 * @author Mário
 */
class Vendas extends Model {

    protected $table = 'vendas';
    
    protected $fillable = 
        [
            'data_pedido',
            'data_entrega',
            'produto_id',
            'cliente_id',
            'created_at',
            'updated_at'
        ];
    
    public function cliente(){
        return $this->belongsTo(Clientes::class);
    }
    
    public function produto(){
        return $this->belongsTo(Produtos::class);
    }

}
