<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Logs extends Model {

    protected $table = 'logs';
    protected $fillable = ['acao', 'ip'];

    //
}
