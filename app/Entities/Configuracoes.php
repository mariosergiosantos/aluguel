<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of Produtos
 *
 * @author Mário
 */
class Configuracoes extends Model {

    protected $table = 'configuracoes';
    
    public $timestamps = false;


    protected $fillable = 
        [
            'user_id',
            'idioma_id',
            'tema_id'
        ];
    
    /**
     * 
     * @return type
     */
    public function user() {
        return $this->belongsTo(User::class);
    }
    
    /**
     * 
     * @return type
     */
    public function idioma() {
        return $this->belongsTo(Idiomas::class);
    }
    
    /**
     * 
     * @return type
     */
    public function tema() {
        return $this->belongsTo(Temas::class);
    }
    
}
