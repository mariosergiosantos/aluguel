<?php

namespace App\Services;

use App\Repositories\UserRepository;
use App\Validator\UserValidator;
use Prettus\Validator\Exceptions\ValidatorException;

class UserServices {

    /**
     *
     * @var type 
     */
    protected $repository;
    /**
     *
     * @var type 
     */
    protected $validator;

    /**
     * 
     * @param UserRepository $repository
     * @param UserValidator $validator
     */
    public function __construct(UserRepository $repository, UserValidator $validator) {
        $this->repository = $repository;
        $this->validator = $validator;
    }

    /**
     * 
     * @param type $id
     * @return type
     */
    public function show($id) {
        try {
            return $this->repository->find($id);
        } catch (\Exception $e) {
            return [
                'error' => 'true',
                'messege' => "Usuário não encontrado!"
            ];
        }
    }

    /**
     * 
     * @param array $data
     * @return type
     */
    public function create(array $data) {
        try {
            $this->validator->with($data)->passesOrFail();
            return $this->repository->create($data);
        } catch (ValidatorException $e) {
            return [
                'error' => 'true',
                'messege' => $e->getMessageBag()
            ];
        }
    }

    /**
     * 
     * @param array $data
     * @param type $id
     * @return type
     */
    public function update(array $data, $id) {
        try {
            $this->validator->with($data)->passesOrFail();
            return $this->repository->update($data, $id);
        } catch (ValidatorException $e) {
            return [
                'error' => 'true',
                'messege' => $e->getMessageBag()
            ];
        } catch (\Exception $e) {
            return [
                'error' => 'true',
                'messege' => "Usuário não encontrado!"
            ];
        }
    }

    /**
     * 
     * @param type $id
     */
    public function delete($id) {
        
    }

}
