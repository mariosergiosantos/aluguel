<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Services;

use App\Repositories\ClienteRepository;
use App\Validator\ClienteValidator;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Description of ClienteServices
 *
 * @author mario
 */
class ClienteServices {

    /**
     *
     * @var type 
     */
    protected $repository;
    /**
     *
     * @var type 
     */
    protected $validator;
    /**
     *
     * @var type 
     */
    private $nofound;

    /**
     * 
     * @param ClienteRepository $repository
     * @param ClienteValidator $validator
     */
    public function __construct(ClienteRepository $repository, ClienteValidator $validator) {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->nofound = trans('messages.nofound.client');
    }

    /**
     * 
     * @param type $id
     * @return type
     */
    public function show($id) {
        try {
            return $this->repository->find($id);
        } catch (\Exception $e) {
            return [
                'error' => 'true',
                'messege' => $this->nofound
            ];
        }
    }

    /**
     * 
     * @param array $data
     * @return type
     */
    public function create(array $data) {
        try {
            $this->validator->with($data)->passesOrFail();
            return $this->repository->create($data);
        } catch (ValidatorException $e) {
            return [
                'error' => 'true',
                'messege' => $e->getMessageBag()
            ];
        }
    }

    /**
     * 
     * @param array $data
     * @param type $id
     * @return type
     */
    public function update(array $data, $id) {
        try {
            $this->validator->with($data)->passesOrFail();
            return $this->repository->update($data, $id);
        } catch (ValidatorException $e) {
            return [
                'error' => 'true',
                'messege' => $e->getMessageBag()
            ];
        } catch (\Exception $e) {
            return [
                'error' => 'true',
                'messege' => $this->nofound
            ];
        }
    }

    /**
     * 
     * @param type $id
     */
    public function delete($id) {
        try {
            $this->repository->delete($id);
        } catch (\Exception $e) {
            return [
                'error' => 'true',
                'messege' => $this->nofound
            ];
        }
    }

}
