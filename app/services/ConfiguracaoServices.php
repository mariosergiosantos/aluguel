<?php

namespace App\Services;

use App\Repositories\ConfiguracaoRepository;
use App\Validator\ConfiguracaoValidator;
use Prettus\Validator\Exceptions\ValidatorException;
use Illuminate\Http\Request;
use App\Entities\Idiomas;

class ConfiguracaoServices {

    /**
     *
     * @var type 
     */
    protected $repository;

    /**
     *
     * @var type 
     */
    protected $validator;
    
    /**
     *
     * @var type 
     */
    private $request;
    
    /**
     *
     * @var type 
     */
    private $idiomas;

    /**
     * 
     * @param Request $request
     * @param ConfiguracaoRepository $repository
     * @param ConfiguracaoValidator $validator
     */
    public function __construct(Request $request, ConfiguracaoRepository $repository, ConfiguracaoValidator $validator, Idiomas $idiomas) {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->request = $request;
        $this->idiomas = $idiomas;
    }

    /**
     * 
     * @param type $id
     * @return type
     */
    public function show($id) {
        try {
            return $this->repository->with(['user', 'idioma', 'tema'])->
                            findWhere(['user_id' => $id])->first();
        } catch (\Exception $e) {
            return [
                'error' => 'true',
                'messege' => $this->nofound
            ];
        }
    }

    /**
     * 
     * @param array $data
     * @return type
     */
    public function create(array $data) {
        try {
            $this->validator->with($data)->passesOrFail();
            return $this->repository->create($data);
        } catch (ValidatorException $e) {
            return [
                'error' => 'true',
                'messege' => $e->getMessageBag()
            ];
        }
    }

    /**
     * 
     * @param array $data
     * @param type $id
     * @return type
     */
    public function update(array $data, $id) {
        try {
            $this->validator->with($data)->passesOrFail();
            $conf = $this->repository->update($data, $id);
            $this->updateLanguage($this->getSiglaLanguage($conf->idioma_id));
        } catch (ValidatorException $e) {
            return [
                'error' => 'true',
                'messege' => $e->getMessageBag()
            ];
        } catch (\Exception $e) {
            return [
                'error' => 'true',
                'messege' => trans('messages.configuration.error')
            ];
        }
    }

    /**
     * 
     * @param type $lang
     */
    public function updateLanguage($lang) {
        if ($this->request->session()->has('language')) {
            $this->request->session()->forget('language');
            $this->request->session()->set("language", $lang);
        }else {
            $this->request->session()->set("language", $lang);
        }
    }
    
    public function getSiglaLanguage($cod){
        $idioma = $this->idiomas->find(['id' => $cod])->first();
        return $idioma->sigla;
    }

}
