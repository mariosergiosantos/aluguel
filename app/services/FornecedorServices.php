<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Services;

use App\Repositories\FornecedorRepository;
use App\Validator\FornecedorValidator;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Description of ClienteServices
 *
 * @author mario
 */
class FornecedorServices {

    /**
     *
     * @var type 
     */
    protected $repository;
    
    /**
     *
     * @var type 
     */
    protected $validator;
    
    private $nofound;

    /**
     * 
     * @param FornecedorRepository $repository
     * @param FornecedorValidator $validator
     */
    public function __construct(FornecedorRepository $repository, FornecedorValidator $validator) {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->nofound = trans('messages.nofound.provider');
    }

    /**
     * 
     * @param type $id
     * @return type
     */
    public function show($id) {
        try {
            return $this->repository->find($id);
        } catch (\Exception $e) {
            return [
                'error' => 'true',
                'messege' => $this->nofound
            ];
        }
    }

    /**
     * 
     * @param array $data
     * @return type
     */
    public function create(array $data) {
        try {
            $this->validator->with($data)->passesOrFail();
            return $this->repository->create($data);
        } catch (ValidatorException $e) {
            return [
                'error' => 'true',
                'messege' => $e->getMessageBag()
            ];
        }
    }

    /**
     * 
     * @param array $data
     * @param type $id
     * @return type
     */
    public function update(array $data, $id) {
        try {
            $this->validator->with($data)->passesOrFail();
            return $this->repository->update($data, $id);
        } catch (ValidatorException $e) {
            return [
                'error' => 'true',
                'messege' => $e->getMessageBag()
            ];
        } catch (\Exception $e) {
            return [
                'error' => 'true',
                'messege' => $this->nofound
            ];
        }
    }

    /**
     * 
     * @param type $id
     */
    public function delete($id) {
        
    }

}
