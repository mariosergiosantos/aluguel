<?php

namespace App\Services;

use App\Repositories\VendaRepository;
use App\Validator\VendaValidator;
use Prettus\Validator\Exceptions\ValidatorException;

class VendasServices {

    /**
     *
     * @var type 
     */
    protected $repository;
    /**
     *
     * @var type 
     */
    protected $validator;
    /**
     *
     * @var type 
     */
    private $nofound;

    /**
     * 
     * @param VendaRepository $repository
     * @param VendaValidator $validator
     */
    public function __construct(VendaRepository $repository, VendaValidator $validator) {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->nofound = trans('messages.nofound.rent');
    }

    /**
     * 
     * @param type $id
     * @return type
     */
    public function show($id) {
        try {
            return $this->repository->find($id);
        } catch (\Exception $e) {
            return [
                'error' => 'true',
                'messege' => $this->nofound
            ];
        }
    }

    /**
     * 
     * @param array $data
     * @return type
     */
    public function create(array $data) {
        try {
            $this->validator->with($data)->passesOrFail();
            return $this->repository->create($data);
        } catch (ValidatorException $e) {
            return [
                'error' => 'true',
                'messege' => $e->getMessageBag()
            ];
        }
    }

    /**
     * 
     * @param array $data
     * @param type $id
     * @return type
     */
    public function update(array $data, $id) {
        try {
            $this->validator->with($data)->passesOrFail();
            return $this->repository->update($data, $id);
        } catch (ValidatorException $e) {
            return [
                'error' => 'true',
                'messege' => $e->getMessageBag()
            ];
        } catch (\Exception $e) {
            return [
                'error' => 'true',
                'messege' => $this->nofound
            ];
        }
    }

    /**
     * 
     * @param type $id
     */
    public function delete($id) {
        
    }

}
