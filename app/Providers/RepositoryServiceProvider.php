<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider {

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot() {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register() {
        $this->app->bind(
                \App\Repositories\ClienteRepository::class, \App\Repositories\ClienteRepositoryEloquent::class
        );

        $this->app->bind(
                \App\Repositories\FornecedorRepository::class, \App\Repositories\FornecedorRepositoryEloquent::class
        );

        $this->app->bind(
                \App\Repositories\ProdutoRepository::class, \App\Repositories\ProdutoRepositoryEloquent::class
        );

        $this->app->bind(
                \App\Repositories\ProdutoVendaRepository::class, \App\Repositories\ProdutoVendaRepositoryEloquent::class
        );

        $this->app->bind(
                \App\Repositories\UserRepository::class, \App\Repositories\UserRepositoryEloquent::class
        );

        $this->app->bind(
                \App\Repositories\VendaRepository::class, \App\Repositories\VendaRepositoryEloquent::class
        );

        $this->app->bind(\App\Repositories\ConfiguracaoRepository::class, \App\Repositories\ConfiguracaoRepositoryEloquent::class
        );
    }

}
