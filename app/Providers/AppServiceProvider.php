<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->loadViewsFrom(base_path('resources/views/painel/pages'), 'page');
        $this->loadViewsFrom(base_path('resources/views/painel/layouts'), 'layouts');
        $this->loadViewsFrom(base_path('resources/views/painel/fragmentos'), 'fragmentos');
        //$this->loadViewsFrom(base_path('resources/views/auth'), 'login');
    }
}
