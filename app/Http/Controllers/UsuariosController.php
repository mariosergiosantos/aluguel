<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use Validator;
use Input;
use Hash;

class UsuariosController extends Controller {

    protected $user = null;

    public function __construct(User $user, Role $role) {
        //$this->middleware('auth');
        //$this->middleware('user.admin');
        $this->user = $user;
        $this->role = $role;
    }

    public function getIndex() {
        $usuarios = $this->user->allUser();
        return view('usuarios.index', compact('usuarios'));
    }

    public function getCadastrar() {
        $roles = $this->role->getRole();
        return view('usuarios.cadastrar', compact('roles'));
    }

    public function postCadastrar() {
        $validator = Validator::make(Input::all(), [
                    'name' => 'required',
                    'password' => 'required|confirmed|min:6',
                    'email' => 'required|email|unique:users'
                        ]
        );

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $salvarUser = $this->user->saveUser();
        if ($salvarUser) {
            return redirect()->to('usuario');
        } else {
            return redirect()->to('usuario')->withErrors('Falha ao realizar cadastro!');
        }
    }

    public function getEditar(Request $request, $id = null) {
        if (empty($id)) {
            $usuario = $this->user->find($request->user()->id);
        } else {
            $usuario = $this->user->find($id);
        }

        if (is_null($usuario)) {
            return redirect()->back()->withErrors('Usuário não encontrado');
        }
        $roles = $this->role->getRole();
        return view('usuarios.editar', compact('usuario', 'roles'));
    }

    public function postEditar(Request $request, $id = null) {
        $validator = Validator::make(Input::all(), [
                    'name' => 'required',
                    'email' => 'required|email',
                    'password' => 'confirmed|min:6',
                        ]
        );

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        if (empty(Input::get('password'))) {
            $input = Input::only('name', 'email');
        } else {
            $input = Input::only('name', 'email', 'password');
            $input['password'] = Hash::make($input['password']);
        }

        if (empty($id)) {
            $updateUser = $this->user->updateUser($request->user()->id, $input);
        } else {
            $updateUser = $this->user->updateUser($id, $input);
        }

        if ($updateUser) {
            return redirect()->back();
        } else {
            return redirect()->back()->withErrors('Usuário não encontrado');
        }
    }

    public function getAtivar($id) {
        $ativarUser = $this->user->ativarUser($id);
        if ($ativarUser) {
            return redirect()->back();
        }
        return redirect()->back()->withErrors('Usuário não encontrado');
    }

    public function getInativar(Request $request, $id) {

        if ($id == $request->user()->id) {
            return redirect()->back()->withErrors('Você não pode se auto bloquear');
        }

        $bloquearUser = $this->user->bloquearUser($id);
        if ($bloquearUser) {
            return redirect()->back();
        }
        return redirect()->back()->withErrors('Usuário não encontrado');
    }

    public function getExcluir($id) {
        $excluirUser = $this->user->deleteUser($id);
        if ($excluirUser) {
            return redirect()->back();
        } else {
            return redirect()->back()->withErrors('Usuário não encontrado');
        }
    }

}
