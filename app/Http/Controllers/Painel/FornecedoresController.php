<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Painel\PainelController;
use App\Repositories\FornecedorRepository as Reposytory;
use App\Services\FornecedorServices as Service;
use Illuminate\Http\Request;

class FornecedoresController extends PainelController {

    /**
     *
     * @var type 
     */
    private $url = "painel/fornecedor";

    /**
     *
     * @var type 
     */
    private $folderView = "page::provider.";

    /**
     *
     * @var type 
     */
    private $fornecedores;

    /**
     *
     * @var type 
     */
    private $service;

    /**
     * 
     * @param Reposytory $fornecedores
     * @param Service $service
     */
    public function __construct(Reposytory $fornecedores, Service $service) {
        $this->fornecedores = $fornecedores;
        $this->service = $service;
    }

    /**
     * 
     * @return type
     */
    public function index() {
        $fornecedores = $this->fornecedores->all();
        return view($this->folderView . 'index', compact('fornecedores'));
    }

    /**
     * 
     * @return type
     */
    public function create() {
        return view($this->folderView . 'cadastrar');
    }

    /**
     * 
     * @param Request $request
     * @return type
     */
    public function store(Request $request) {
        $cliente = $this->service->create($request->all());
        if ($cliente['error']) {
            return redirect($this->url)->with("danger", $cliente['messege']);
        }
        return redirect($this->url)->with("success", trans('messages.store.provider'));
    }

    /**
     * 
     * @param type $id
     * @return type
     */
    public function show($id) {
        $fornecedor = $this->service->show($id);
        if ($fornecedor['error']) {
            return redirect($this->url)->with("danger", $fornecedor['messege']);
        }
        return view($this->folderView . 'detalhar', compact('fornecedor'));
    }

    /**
     * 
     * @param type $id
     * @return type
     */
    public function edit($id) {
        $fornecedor = $this->service->show($id);
        if ($fornecedor['error']) {
            return redirect($this->url)->with("danger", $fornecedor['messege']);
        }
        return view($this->folderView . 'editar', compact('fornecedor'));
    }

    /**
     * 
     * @param Request $request
     * @param type $id
     */
    public function update(Request $request, $id) {
        $fornecedor = $this->service->show($id);
        if ($fornecedor['error']) {
            return redirect($this->url)->with("danger", $fornecedor['messege']);
        }
        $this->service->update($request->all(), $id);
        return redirect($this->url)->with("success", trans('messages.update.provider'));
    }

    /**
     * 
     * @param type $id
     */
    public function destroy($id) {
        $fornecedor = $this->service->show($id);
        if ($fornecedor['error']) {
            return redirect($this->url)->with("danger", $fornecedor['messege']);
        }
        $this->service->delete($id);
        return redirect($this->url)->with("success", trans('messages.destroy.provider'));
    }

}
