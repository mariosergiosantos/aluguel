<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Painel\PainelController;
use App\Entities\Vendas;
use App\Repositories\VendaRepository as Repository;
use App\Repositories\ProdutoRepositoryEloquent as ProdutoRepository;
use App\Repositories\ClienteRepositoryEloquent as ClienteRepository;
use App\Services\VendasServices as Service;
use Illuminate\Http\Request;

class VendasController extends PainelController {

    private $url = "painel/aluguel";

    /**
     *
     * @var type 
     */
    private $repository;

    /**
     *
     * @var type 
     */
    private $service;
    
    private $folderView = "page::rent.";

    /**
     * 
     * @param Vendas $vendas
     */
    public function __construct(Repository $vendas, Service $service) {
        $this->repository = $vendas;
        $this->service = $service;
    }

    /**
     * 
     * @return type
     */
    public function index() {
        $vendas = $this->repository->with(['cliente', 'produto'])->all();
        return view($this->folderView . 'index', compact('vendas'));
    }

    /**
     * 
     * @return type
     */
    public function create(ProdutoRepository $produto, ClienteRepository $cliente) {
        $produtos = $produto->all();
        $clientes = $cliente->all();
        return view($this->folderView . 'cadastrar', compact('produtos', 'clientes'));
    }

    /**
     * 
     * @param Request $request
     * @return type
     */
    public function store(Request $request) {
        $aluguel = $this->service->create($request->all());
        if ($aluguel['error']) {
            return redirect($this->url)->with("danger", $aluguel['messege']);
        }
        return redirect($this->url)->with("success", trans('messages.store.rent'));
    }

    /**
     * 
     * @param type $id
     * @return type
     */
    public function show($id) {
        $fornecedor = $this->service->show($id);
        return view($this->folderView . 'detalhar', compact('fornecedor'));
    }

    /**
     * 
     * @param type $id
     * @return type
     */
    public function edit($id) {
        $fornecedor = $this->service->show($id);
        return view($this->folderView . 'editar', compact('fornecedor'));
    }

    /**
     * 
     * @param Request $request
     * @param type $id
     */
    public function update(Request $request, $id) {
        $this->service->update($request->all(), $id);
    }

    /**
     * 
     * @param type $id
     */
    public function destroy($id) {
        $this->service->delete($id);
    }

}
