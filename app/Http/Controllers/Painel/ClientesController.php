<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Painel\PainelController;
use App\Services\ClienteServices as Service;
use App\Repositories\ClienteRepository as Repository;
use Illuminate\Http\Request;

class ClientesController extends PainelController {

    protected $url = "painel/cliente";

    /**
     *
     * @var type 
     */
    protected $folderView = "page::client.";

    /**
     *
     * @var type 
     */
    private $repository;

    /**
     *
     * @var type 
     */
    private $service;

    /**
     * 
     * @param Clientes $clientes
     */
    public function __construct(Repository $cliente, Service $service) {
        $this->repository = $cliente;
        $this->service = $service;
    }

    /**
     * 
     * @return type
     */
    public function index() {
        $clientes = $this->repository->paginate(10);
        return view($this->folderView . 'index', compact('clientes'));
    }

    /**
     * 
     * @return type
     */
    public function create() {
        return view($this->folderView . 'cadastrar');
    }

    /**
     * 
     * @param Request $request
     * @return type
     */
    public function store(Request $request) {
        $cliente = $this->service->create($request->all());
        if ($cliente['error']) {
            return redirect($this->url)->with("danger", $cliente['messege']);
        }
        return redirect($this->url)->with("success", trans('messages.store.client'));
    }

    /**
     * 
     * @param type $id
     * @return type
     */
    public function show($id) {
        $cliente = $this->service->show($id);
        if ($cliente['error']) {
            return redirect($this->url)->with("danger", $cliente['messege']);
        }
        return view($this->folderView . 'detalhar', compact('cliente'));
    }

    /**
     * 
     * @param type $id
     * @return type
     */
    public function edit($id) {
        $cliente = $this->service->show($id);
        if ($cliente['error']) {
            return redirect($this->url)->with("danger", $cliente['messege']);
        }
        return view($this->folderView . 'editar', compact('cliente'));
    }

    /**
     * 
     * @param Request $request
     * @param type $id
     */
    public function update(Request $request, $id) {
        $cliente = $this->service->show($id);
        if ($cliente['error']) {
            return redirect($this->url)->with("danger", $cliente['messege']);
        }
        $this->service->update($request->all(), $id);
        return redirect($this->url)->with("success", trans('messages.update.client'));
    }

    /**
     * 
     * @param type $id
     */
    public function destroy($id) {
        $cliente = $this->service->show($id);
        if ($cliente['error']) {
            return redirect($this->url)->with("danger", $cliente['messege']);
        }
        $this->service->delete($id);
        return redirect($this->url)->with("success", trans('messages.destroy.client'));
    }

}
