<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Painel\PainelController;
use App\Entities\Produtos;
use App\Entities\Categorias;
use Illuminate\Http\Request;
//use App\Repositories\ProdutoRepository as Produtos;
use App\Repositories\FornecedorRepository as Fornecedores;
use App\Services\ProdutoServices as Service;

class ProdutosController extends PainelController {

    /**
     *
     * @var type 
     */
    private $url = "painel/produto";

    /**
     *
     * @var type 
     */
    private $folderView = "page::product.";

    /**
     *
     * @var type 
     */
    private $produtos;

    /**
     *
     * @var type 
     */
    private $service;

    /**
     * 
     * @param Produtos $produtos
     */
    public function __construct(Produtos $produtos, Service $service) {
        $this->produtos = $produtos;
        $this->service = $service;
    }

    /**
     * 
     * @return type
     */
    public function index() {
        $produtos = $this->produtos
                ->join('categorias', 'produtos.fk_categoria', '=', 'categorias.id')
                ->join('fornecedores', 'produtos.fk_fornecedor', '=', 'fornecedores.id')
                ->select('produtos.id', 'produtos.nome as nome_produto', 'produtos.descricao', 'produtos.preco_unitario', 'produtos.estoque', 'categorias.nome as nome_categoria', 'fornecedores.nome as nome_fornecedor')
                ->paginate(10);
        return view($this->folderView . 'index', compact('produtos'));
        //$produtos = $this->produtos->with(['categoria', 'fornecedor'])->paginate(3);
        //return view('painel::paginas.produtos', compact('produtos'));
        //return response()->json($produtos);
    }

    /**
     * 
     * @param Fornecedores $fornecedor
     * @param Categorias $categoria
     * @return type
     */
    public function create(Fornecedores $fornecedor, Categorias $categoria) {
        $fornecedores = $fornecedor->all();
        $categorias = $categoria->all();
        return view($this->folderView . 'cadastrar', compact('fornecedores', 'categorias'));
    }

    /**
     * 
     * @param Request $request
     * @return type
     */
    public function store(Request $request) {
        $produto = $this->service->create($request->all());
        if ($produto['error']) {
            return redirect($this->url)->with("danger", $produto['messege']);
        }
        return redirect($this->url)->with("success", trans('messages.store.product'));
    }

    /**
     * 
     * @param type $idProduto
     * @return type
     */
    public function show($idProduto) {
        $produto = $this->service->show($idProduto);
        if ($produto['error']) {
            return redirect($this->url)->with("danger", $produto['messege']);
        }
        return view($this->folderView . 'detalhar', compact('produto'));
    }

    /**
     * 
     * @param type $id
     * @return type
     */
    public function edit($id, Fornecedores $fornecedor, Categorias $categoria) {
        $produto = $this->service->show($id);
        if ($produto['error']) {
            return redirect($this->url)->with("danger", $produto['messege']);
        }
        $fornecedores = $fornecedor->all();
        $categorias = $categoria->all();
        return view($this->folderView . 'editar', compact('produto', 'fornecedores', 'categorias'));
    }

    /**
     * 
     * @param Request $request
     * @param type $id
     */
    public function update(Request $request, $id) {
        $produto = $this->service->show($id);
        if ($produto['error']) {
            return redirect($this->url)->with("danger", $produto['messege']);
        }
        $this->service->update($request->all(), $id);
        return redirect($this->url)->with("success", trans('messages.update.product'));
    }

    /**
     * 
     * @param type $id
     */
    public function destroy($id) {
        $produto = $this->service->show($id);
        if ($produto['error']) {
            return redirect($this->url)->with("danger", $produto['messege']);
        }
        $this->service->delete($id);
        return redirect($this->url)->with("success", trans('messages.destroy.product'));
    }
}
