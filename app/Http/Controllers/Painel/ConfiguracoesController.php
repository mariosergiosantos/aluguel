<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Painel\PainelController;
use App\Services\ConfiguracaoServices as Service;
use App\Repositories\ConfiguracaoRepository as Repository;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard as Auth;
use App\Entities\Temas;
use App\Entities\Idiomas;

class ConfiguracoesController extends PainelController {

    /**
     *
     * @var type 
     */
    private $url = "painel/configuracao";

    /**
     *
     * @var type 
     */
    private $folderView = "page::configuration.";

    /**
     *
     * @var type 
     */
    private $repository;

    /**
     *
     * @var type 
     */
    private $service;

    /**
     *
     * @var type 
     */
    private $auth;

    /**
     * 
     * @param Clientes $clientes
     */
    public function __construct(Repository $repository, Service $service, Auth $auth) {
        $this->repository = $repository;
        $this->service = $service;
        $this->auth = $auth;
    }

    public function index() {
        $configuracao = $this->service->show($this->auth->user()->id);
        return view($this->folderView . 'index', compact('configuracao'));
    }

    /**
     * 
     * @param type $id
     * @return type
     */
    public function edit(Temas $tema, Idiomas $idioma) {
        $temas = $tema->all();
        $idiomas = $idioma->all();
        return view($this->folderView . 'editar', compact('temas', 'idiomas'));
    }

    /**
     * 
     * @param Request $request
     * @param type $id
     */
    public function update(Request $request) {
        $configuracao = $this->service->show($this->auth->user()->id);
        $error = $this->service->update($request->all(), $configuracao->id);
        if ($error['error']) {
            return redirect($this->url)->with("danger", $error['messege']);
        }
        return redirect($this->url)->with("success", trans('messages.configuration.success'));
    }
}
