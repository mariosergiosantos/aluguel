<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Painel\PainelController;
//use App\Entities\ProdutosVendas;
use App\Entities\Fornecedores;
use App\Entities\Produtos;
use App\Entities\Clientes;
use App\Entities\Vendas;
use Illuminate\Contracts\Auth\Guard as Auth;

class DashboardController extends PainelController {

    /**
     *
     * @var type 
     */
    private $fornecedores;
    /**
     *
     * @var type 
     */
    private $produtos;
    /**
     *
     * @var type 
     */
    private $clientes;
    /**
     *
     * @var type 
     */
    private $vendas;
    /**
     *
     * @var type 
     */
    private $auth;
    
    /**
     * 
     * @param Fornecedores $fornecedores
     * @param Produtos $produtos
     * @param Clientes $clientes
     * @param Vendas $vendas
     * @param Auth $auth
     */
    public function __construct(Fornecedores $fornecedores, Produtos $produtos, Clientes $clientes, Vendas $vendas, Auth $auth) {
      $this->fornecedores = $fornecedores;
      $this->produtos = $produtos;
      $this->clientes = $clientes;
      $this->vendas = $vendas;
      $this->auth = $auth;
    }

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function index() {
      $fornecedores = $this->fornecedores->all();
      $produtos = $this->produtos->all();
      $clientes = $this->clientes->all();
      $vendas = $this->vendas->all();
      /*$ultimosProdutosVendidos = $this->produtosVendas
      ->join('produtos', 'produtos.id', '=', 'produtos_vendas.produto_id')
      ->join('vendas', 'vendas.id', '=', 'produtos_vendas.venda_id')
      ->select('produtos.id', 'produtos.nome', 'vendas.created_at as horario')
      ->take(10)->get();*/
      return view('page::dashboard', compact('fornecedores', 'produtos', 'clientes', 'vendas'));
    }

  }
