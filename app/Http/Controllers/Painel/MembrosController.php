<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Painel\PainelController;
use App\Entities\User;

class MembrosController extends PainelController {

    /**
     *
     * @var type 
     */
    private $membros;

    /**
     * 
     * @param User $membros
     */
    public function __construct(User $membros) {
        $this->membros = $membros;
    }

    /**
     * 
     * @return type
     */
    public function index() {
        $membros = $this->membros->all();  
        return view('painel::paginas.membros', compact('membros'));
    }
    
    /**
     * 
     * @return type
     */
    public function create(){
        return view('painel::paginas.cadastrar-membro');
    }
    
    /**
     * 
     * @param Request $request
     * @return type
     */
    public function store(Request $request){
        return $this->service->create($request->all());
    }
    
    /**
     * 
     * @param type $id
     * @return type
     */
    public function show($id){
        $membro = $this->service->show($id);
        return view('painel::paginas.detalhar-membro', compact('membro'));
    }
    
    /**
     * 
     * @param type $id
     * @return type
     */
    public function edit($id){
        $membro = $this->service->show($id);
        return view('painel::paginas.editar-detalhes', compact('membro'));
    }
    
    
    /**
     * 
     * @param Request $request
     * @param type $id
     */
    public function update(Request $request, $id){
        $this->service->update($request->all(), $id);
    }
    
    /**
     * 
     * @param type $id
     */
    public function destroy($id){
        $this->service->delete($id);
    }
    
    /**
     * 
     * @param type $q
     * @return type
     */
    public function search($q){
        return null;
    }

}
