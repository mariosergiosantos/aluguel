<?php

namespace App\Http\Controllers\Auth;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Socialite;
use App\Http\Controllers\Controller;
use App\Repositories\UserRepository as User;
use Illuminate\Contracts\Auth\Guard as Auth;
use Log;

/**
 * Description of OAuthController
 *
 * @author mario
 */
class OAuthController extends Controller {

    /**
     *
     * @var type 
     */
    private $user;

    /**
     *
     * @var type 
     */
    private $auth;

    /**
     * 
     * @param User $user
     * @param Auth $auth
     */
    public function __construct(User $user, Auth $auth) {
        $this->user = $user;
        $this->auth = $auth;
    }

    /**
     * Redirect the user to the Google authentication page.
     *
     * @return Response
     */
    public function redirectToProviderGoogle() {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Obtain the user information from Google.
     *
     * @return Response
     */
    public function handleProviderCallbackGoogle() {
        $userOauth = Socialite::driver('google')->user();
        $userApp = $this->user->findWhere(['email' => $userOauth->email])->first();

        if ($userApp == null) {
            $data = [
                "name" => $userOauth->name,
                "email" => $userOauth->email,
                "status" => "1",
                "url_image" => $userOauth->avatar
            ];
            $user = $this->user->create($data);
            \App\Entities\Configuracoes::create(['user_id' => $user->id]);
            $this->auth->loginUsingId($user->id);
            return redirect("/");
        } else {
            Log::info('Showing user profile for user: '.$userApp->id);
            $this->auth->loginUsingId($userApp->id);
            return redirect("/");
        }
    }

}
