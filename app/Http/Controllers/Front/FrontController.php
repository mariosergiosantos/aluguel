<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;

abstract class FrontController extends Controller {
    # implemente as suas regras e/ou métodos que serão válidas/usadas em todos os controllers do seu "front"
}
