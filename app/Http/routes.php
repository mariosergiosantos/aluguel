<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

Route::get('/', function () {
    return Redirect::to('auth/login');
});

Route::get('google', function(){
    //$analyticsData = LaravelAnalytics::getVisitorsAndPageViews(7);
});

Route::group(['prefix' => 'painel', 'namespace' => "Painel", 'middleware' => 'auth'], function() {
    Route::get('dashboard', 'DashboardController@index');
    Route::resource('fornecedor', 'FornecedoresController');
    Route::resource('cliente', 'ClientesController');
    Route::resource('produto', 'ProdutosController');
    Route::resource('membro', 'MembrosController');
    Route::resource('aluguel', 'VendasController');
    Route::get('configuracao', 'ConfiguracoesController@index');
    Route::get('configuracao/edit', 'ConfiguracoesController@edit');
    Route::put('configuracao/edit', 'ConfiguracoesController@update');
});

Route::get('auth/google', 'Auth\OAuthController@redirectToProviderGoogle');
Route::get('auth/google/callback', 'Auth\OAuthController@handleProviderCallbackGoogle');

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
    'usuario' => 'UsuariosController',
]);

Route::get('locate/{locale}', function ($locale) {
    App::setLocale($locale);
    
    return trans('messages.store.provider');
    //return redirect('painel/dashboard');
});

Event::listen('illuminate.query', function ($sql) {
    //print_R($sql);  
});
