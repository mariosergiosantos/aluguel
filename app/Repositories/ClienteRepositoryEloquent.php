<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Entities\Clientes;

/**
 * Description of ClienteRepositoryEloquent
 *
 * @author mario
 */
class ClienteRepositoryEloquent extends BaseRepository implements ClienteRepository {
    
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nome' => 'like',
        'email' => 'like',
        'telefone',
        'documento',
        'estado',
        'cidade',
        'pais',
        'endereco' => 'like'
    ];

    public function boot() {
        $this->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
    }

    public function model(){
        return Clientes::class;
    }
}
