<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Entities\Vendas;

/**
 * Description of UserRepositoryEloquent
 *
 * @author mario
 */
class VendaRepositoryEloquent extends BaseRepository implements VendaRepository {

    public function model(){
        return Vendas::class;
    }
}
