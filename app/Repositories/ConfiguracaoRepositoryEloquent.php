<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Entities\Configuracoes;

/**
 * Description of UserRepositoryEloquent
 *
 * @author mario
 */
class ConfiguracaoRepositoryEloquent extends BaseRepository implements ConfiguracaoRepository {

    public function model(){
        return Configuracoes::class;
    }
}
