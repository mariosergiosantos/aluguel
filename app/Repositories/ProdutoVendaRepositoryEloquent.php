<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Entities\ProdutosVendas;

/**
 * Description of ProdutoRepositoryEloquent
 *
 * @author mario
 */
class ProdutoVendaRepositoryEloquent extends BaseRepository implements ProdutoVendaRepository {

    public function model(){
        return ProdutosVendas::class;
    }
}
