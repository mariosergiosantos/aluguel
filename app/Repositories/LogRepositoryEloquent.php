<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Entities\Logs;

/**
 * Description of LogRepositoryEloquent
 *
 * @author mario
 */
class LogRepositoryEloquent extends BaseRepository implements LogRepository {

    public function model(){
        return Logs::class;
    }
}
