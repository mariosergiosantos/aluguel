<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Entities\Produtos;

/**
 * Description of ProdutoRepositoryEloquent
 *
 * @author mario
 */
class ProdutoRepositoryEloquent extends BaseRepository implements ProdutoRepository {

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nome' => 'like',
        'descricao' => 'like'
    ];

    public function boot() {
        $this->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
    }

    public function model() {
        return Produtos::class;
    }

}
