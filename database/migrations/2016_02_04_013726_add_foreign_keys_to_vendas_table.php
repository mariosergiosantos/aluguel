<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToVendasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('vendas', function(Blueprint $table)
		{
			$table->foreign('cliente_id', 'add_foreign_vendas_cliente')->references('id')->on('clientes')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('produto_id', 'add_foreign_vendas_produto')->references('id')->on('produtos')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('vendas', function(Blueprint $table)
		{
			$table->dropForeign('add_foreign_vendas_cliente');
			$table->dropForeign('add_foreign_vendas_produto');
		});
	}

}
