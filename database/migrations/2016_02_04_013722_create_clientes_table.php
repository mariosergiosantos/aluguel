<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClientesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('clientes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nome', 60);
			$table->string('email', 60)->unique();
			$table->string('telefone', 60);
			$table->string('documento', 20);
			$table->string('estado', 80);
			$table->string('cidade', 80);
			$table->string('pais', 100);
			$table->string('cep', 10);
			$table->string('endereco', 60);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('clientes');
	}

}
