<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToProdutosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('produtos', function(Blueprint $table)
		{
			$table->foreign('fk_categoria', 'categoria_produto_foreign')->references('id')->on('categorias')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('fk_fornecedor', 'fornecedor_produto_foreign')->references('id')->on('fornecedores')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('produtos', function(Blueprint $table)
		{
			$table->dropForeign('categoria_produto_foreign');
			$table->dropForeign('fornecedor_produto_foreign');
		});
	}

}
