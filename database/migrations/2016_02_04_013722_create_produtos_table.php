<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProdutosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('produtos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nome');
			$table->string('descricao');
			$table->string('url_img');
			$table->integer('fk_categoria')->unsigned()->index('fk_categoria');
			$table->integer('fk_fornecedor')->unsigned()->index('fk_fornecedor');
			$table->float('preco_unitario');
			$table->integer('estoque');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('produtos');
	}

}
