<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateImagensProdutoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('imagens_produto', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nome', 60);
			$table->string('url', 80);
			$table->integer('produto_id')->unsigned()->index('produto_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('imagens_produto');
	}

}
