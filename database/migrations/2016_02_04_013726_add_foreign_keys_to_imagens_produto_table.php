<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToImagensProdutoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('imagens_produto', function(Blueprint $table)
		{
			$table->foreign('produto_id', 'imagens_produto_produto')->references('id')->on('produtos')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('imagens_produto', function(Blueprint $table)
		{
			$table->dropForeign('imagens_produto_produto');
		});
	}

}
