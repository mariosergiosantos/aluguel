<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToConfiguracoesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('configuracoes', function(Blueprint $table)
		{
			$table->foreign('idioma_id', 'foreign_configuracoes_idioma')->references('id')->on('idiomas')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('tema_id', 'foreign_configuracoes_tema')->references('id')->on('temas')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('user_id', 'foreign_configuracoes_user')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('configuracoes', function(Blueprint $table)
		{
			$table->dropForeign('foreign_configuracoes_idioma');
			$table->dropForeign('foreign_configuracoes_tema');
			$table->dropForeign('foreign_configuracoes_user');
		});
	}

}
